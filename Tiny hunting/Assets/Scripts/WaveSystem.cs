using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSystem : MonoBehaviour
{
    public List<Transform> spawnerPoints;
    public List<Prison> prisonPrefab;

    //public Vector3 target;
    // Start is called before the first frame update
    void Start()
    {
        //spawner();
        StartCoroutine(createWave());
    }
    IEnumerator createWave()
    {
        
        for (int i = 0; i < 5; i++)
        {
            yield return new WaitForSeconds(5);
            Prison employeeObj = Instantiate(prisonPrefab[i], spawnerPoints[i]);

            //employeeObj.target = target;
            int spawnPoint = Random.Range(0, spawnerPoints.Count);
            //employeeObj.transform.localPosition = spawnerPoints[spawnPoint].position;
        }
        StartCoroutine(createWave());
    }

    //private void spawner()
    //{
    //    for (int i = 0; i < 5; i++)
    //    {
            
    //        Prison employeeObj = Instantiate(prisonPrefab[i], spawnerPoints[i]);

    //        employeeObj.target = target;
    //        int spawnPoint = Random.Range(0, spawnerPoints.Count);
    //        employeeObj.transform.localPosition = spawnerPoints[spawnPoint].position;
    //    }
    //    StartCoroutine(createWave());
    //}
}
