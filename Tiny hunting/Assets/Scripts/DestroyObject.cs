﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(destroyer());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    IEnumerator destroyer()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(this.gameObject);
    }
}
