﻿
using UnityEngine;

public class Interactable : MonoBehaviour {

    public float radius = 3f;
    bool isFocus = false;
    Transform player;
    public Transform interactionTransform;
    bool hasInteracted = false;

    public virtual void Interact()
    {
        Debug.Log("interact with player !");
        Debug.Log(this.gameObject.transform.position);
    }


    void Update()
    {
        if (isFocus && !hasInteracted)
        {
            float distance = Vector3.Distance(player.position, interactionTransform.position);
            if (distance <= radius)
            {
                Interact();
                Debug.Log("INTERACT");
                hasInteracted = true;

            }
        }
    }
    public void OnFocused(Transform playerTransform)
    {
        isFocus = true;
        player = playerTransform;
        hasInteracted = false;

    }

    public void OnDefocused()
    {
        Debug.Log("OnDefocused");
        isFocus = false;
        player = null;
        hasInteracted = false;


    }

    void OnDrawGizmosSelected()
    {
        
        if (interactionTransform == null)
            interactionTransform = transform;
        // Display the explosion radius when selected
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }
	
}
