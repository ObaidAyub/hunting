﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(NavMeshAgent))]
public class PlayerMotor : MonoBehaviour {

    public Transform target;
    NavMeshAgent agent;

    public float lookRadius;

    public static bool playerDie = false;

    CharacterCombat combat;
    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        combat = GetComponent<CharacterCombat>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Employee")
        {
            target = other.transform;
        }
    }

    void FixedUpdate()
    {
        //target = FindObjectOfType<Prison>();
        if (!playerDie)
        {
            if (target != null)
            {
                //Debug.Log("Is working 1");
                float distance = Vector3.Distance(target.transform.position, transform.position);
                FaceTarget();
                Vector3 direction = (target.transform.position - transform.position).normalized;

                if (target.transform.tag == "Enemy")
                {
                    if (distance <= lookRadius)
                    {
                        CharacterStats targetStats = target.GetComponent<CharacterStats>();
                        combat.Attack(targetStats);
                        CharacterAnimation.instance.PlayerAttackAnimStart();
                    }
                }
                if (target.transform.tag == "Employee")
                {
                    if (distance <= lookRadius)
                    {
                        //Debug.Log("Is working");
                        CharacterAnimation.instance.run(true);
                        //agent.Move(target.transform.position);
                        //CharacterAnimation.instance.run(false);
                        CharacterStats targetStats = target.GetComponent<CharacterStats>();
                        //Debug.Log("Current Health " + targetStats.currentHealth);
                        if (targetStats.currentHealth > 0)
                        {
                        combat.Attack(targetStats);
                        CharacterAnimation.instance.PlayerAttackAnimStart();
                        }
                        else
                        {
                            target = null;
                            CharacterAnimation.instance.PlayerAttackAnimStop();
                            //Debug.Log("Current Health " + targetStats.currentHealth);
                            return;
                        }
                    }
                }
                //if (target.transform.tag == "Gate")
                //{
                //    if (distance <= lookRadius)
                //    {
                //        CharacterStats targetStats = target.GetComponent<CharacterStats>();
                //        combat.Attack(targetStats);
                //        CharacterAnimation.instance.PlayerAttackAnimStart();
                //    }
                //}
            }
            else
            {

                CharacterAnimation.instance.PlayerAttackAnimStop();
            }
        }
    }
    public void MoveToPoint(Vector3 point)
    {
        //agent.SetDestination(point);
        //Debug.Log("MoveToPoint");
        agent.Move(point*Time.deltaTime*agent.speed);
    }



    //public void FollowTarget(Interactable newTarget)
    //{
    //    Debug.Log("Follow target");
    //    agent.stoppingDistance = newTarget.radius * .8f;
    //    agent.updateRotation = false;
    //    //target = newTarget.interactionTransform;
    //    //target = newTarget.interactionTransform;

    //}


    //public void StopFollowingTarget()
    //{
    //    Debug.Log("StopFollowingTarget");
    //    agent.stoppingDistance = 1.5f;
    //    agent.updateRotation = true;
    //    target = null;  

    //}

    public void FaceTarget()
    {
        Vector3 direction = (target.transform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}


