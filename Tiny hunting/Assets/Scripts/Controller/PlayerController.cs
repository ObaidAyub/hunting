﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UIElements;
using CnControls;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour {
    
    public Interactable focus;
    Camera cam;
    public LayerMask movementMask;
    PlayerMotor motor;
    public Transform HitPointPrefab;
    public Transform enemyHitPointPrefab;
    public float hitpoinposition;

    private Transform _mainCameraTransform;
    private Transform _transform;

    // Use this for initialization
    void Start () {
	    
        cam = Camera.main;
        motor = GetComponent<PlayerMotor>();
        _mainCameraTransform = Camera.main.GetComponent<Transform>();
        _transform = GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update() {

        //if (EventSystem.current.IsPointerOverGameObject())
        //    return;

        //if (Input.touchCount > 0)
        //{
        //    Touch touch = Input.GetTouch(0);

        //    // Move the cube if the screen has the finger moving.
        //    if (touch.phase == TouchPhase.Moved)
        //    {
        //        Debug.LogError("touch Input");
        //        PlayerRayCasting();
        //    }

        //}
        if (Input.GetMouseButtonDown(0))
        {
            //PlayerRayCasting();
        }
        //if (Input.GetMouseButtonDown(1))
        //{
        //    Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        //    RaycastHit hit;
        //    if (Physics.Raycast(ray, out hit, 100))
        //    {
        //        Interactable interactable = hit.collider.GetComponent<Interactable>();

        //        if(interactable != null)
        //        {
        //            SetFocus(interactable);
        //        }
        //    }

        //}

        // Just use CnInputManager. instead of Input. and you're good to go
        var inputVector = new Vector3(CnInputManager.GetAxis("Horizontal"), CnInputManager.GetAxis("Vertical"));
        Vector3 movementVector = Vector3.zero;

        // If we have some input
        if (inputVector.sqrMagnitude > 0.001f)
        {
            CharacterAnimation.instance.run(true);
            movementVector = _mainCameraTransform.TransformDirection(inputVector);
            movementVector.y = 0f;
            movementVector.Normalize();
            _transform.forward = movementVector;
        }
        else
        {
            CharacterAnimation.instance.run(false);

        }
        movementVector += Physics.gravity;
        motor.MoveToPoint(movementVector);




        if (movementVector.magnitude <= 0f)
        {
            Debug.Log("charcter is in idle position");
        }

    }
    /*
    void SetFocus(Interactable newFocus)
    {
        
        //if (newFocus != focus)
        //{
            
        //    if (focus != null)
        //        focus.OnDefocused();

        //    focus = newFocus;
            motor.FollowTarget(newFocus);
           
            
        //}

        //newFocus.OnFocused(transform);

    }

    void RemoveFocus()
    {
        Debug.Log("REmoveFocus");

        if (focus != null)
            focus.OnDefocused();

        focus = null;
        motor.StopFollowingTarget();
    }

    private void PlayerRayCasting()
    {
        
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            Debug.Log("PlayerMove" + hit.collider.name + "  " + hit.collider.tag);
            if (hit.collider.tag == "Extraction point")
            {
                Debug.LogError("Level Clear ");
                // do cheer animation 
                // show level clear UI
            }
            else if (hit.collider.tag == "Gate")
            {
                Instantiate(enemyHitPointPrefab, new Vector3(hit.point.x, hitpoinposition, hit.point.z), Quaternion.identity);
            }
            else if (hit.collider.tag == "Enemy")
            {
                Instantiate(enemyHitPointPrefab, new Vector3(hit.point.x, hitpoinposition, hit.point.z), Quaternion.identity);
            }
            else
            {
                Instantiate(HitPointPrefab, new Vector3(hit.point.x, hitpoinposition, hit.point.z), Quaternion.identity);
            }
            //motor.MoveToPoint(hit.point);
            RemoveFocus();
        }


        // from this ray cast play get their target
        if (Physics.Raycast(ray, out hit, 100))
        {
            Interactable interactable = hit.collider.GetComponent<Interactable>();

            if (interactable != null)
            {
                SetFocus(interactable); 
                
            }
        }
    }
    */
}

