﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public float lookRadius = 10f;  // Detection range for player

    Transform target;   // Reference to the player
    NavMeshAgent agent; // Reference to the NavMeshAgent
    CharacterCombat combat;
    Animator animator;   
    const float locomotionAnimationSmoothTime = 0.1f;
    public bool AttackPlayer;

    public bool die;

    CharacterStats enemystats;

    // Use this for initialization
    void Start()
    {
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        combat = GetComponent<CharacterCombat>();
        animator = GetComponentInChildren<Animator>();
        enemystats =  this.gameObject.GetComponent<CharacterStats>();
    }

    // Update is called once per frame
    void Update()
    {


        if (target != null)
        {
            if (enemystats.currentHealth > 0)
            {               
                float distance = Vector3.Distance(target.position, transform.position);
               
                if (distance <= lookRadius)
                {
                    FaceTarget();

                    if (distance >= agent.stoppingDistance)
                    {
                        animator.SetBool("Attack", false);
                        agent.SetDestination(target.position);
                        float speedPercent = agent.velocity.magnitude / agent.speed;
                        animator.SetFloat("speed", speedPercent, locomotionAnimationSmoothTime, Time.deltaTime);

                    }
                }
                if (distance >= lookRadius)
                {
                    float speedPercent = agent.velocity.magnitude / agent.speed;
                    animator.SetFloat("speed", speedPercent, locomotionAnimationSmoothTime, Time.deltaTime);
                }
                if (distance <= agent.stoppingDistance)
                {
                    CharacterStats targetStats = target.GetComponent<CharacterStats>();
                    Debug.LogError("Working");
                    if (targetStats != null)
                    {
                        combat.Attack(targetStats);
                        Debug.Log("attack the player");
                        animator.SetBool("Attack", true);
                    }

                    if (distance > agent.stoppingDistance)
                    {
                        Debug.Log("stop the player");
                        animator.SetBool("Attack", false);
                    }
                }
            }
            else
            {
                Debug.LogError("now Enemy die");
                die = true;
            }
        }
        else
        {
            Debug.Log("rest now");
            animator.SetBool("Attack", false);
            float speedPercent = agent.velocity.magnitude / agent.speed;
            animator.SetFloat("SpeedPercent", speedPercent - Time.deltaTime, locomotionAnimationSmoothTime, Time.deltaTime);


        }

    }

    // Rotate to face the target
    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }
   
    // Show the lookRadius in editor
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}
