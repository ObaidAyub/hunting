﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;

public class Prison : MonoBehaviour {
    public Vector3 target;
    public NavMeshAgent agent;
    CharacterStats characterStats;

    public GameObject gate;

    public float lookRadius = 1;
    public static CharacterAnimation instance;

    Animator animator;
    const float locomotionAnimationSmoothTime = 0.1f;


    public GameObject DestinationPoint;

    public bool _die = false;
    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        characterStats = GetComponent<CharacterStats>();
        //gate = GameObject.FindGameObjectWithTag("Gate");
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_die)
        {
            float speedPercent = agent.velocity.magnitude / agent.speed;
            //Debug.Log(speedPercent);
            animator.SetFloat("speed", speedPercent, locomotionAnimationSmoothTime, Time.deltaTime);

            agent.SetDestination(target);

            if (characterStats.currentHealth <= 0)
            {
                PlayerDieAnim();
            }

            float distance = Vector3.Distance(target, transform.position);

            //if (distance <= lookRadius)
            //{
            //    DestinationPoint.SetActive(true);

            //}
            //agent.pathEndPosition

        }
        else
        {
            //Debug.Log("destination Point is null");
        }
    }

    public void PlayerDieAnim()
    {
        animator.SetBool("Die", true);
        StartCoroutine(waitforDeath());
    }
    public void PlayercheerAnim()
    {
        animator.SetBool("Cheer", true);
        StartCoroutine(waitforDeath());
    }

    public IEnumerator waitforDeath()
    {
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);

    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Extraction point")
        {
            Destroy(this.gameObject);
        }
    }
}

