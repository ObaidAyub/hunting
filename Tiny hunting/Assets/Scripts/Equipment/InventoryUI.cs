﻿
using UnityEngine;

public class InventoryUI : MonoBehaviour {

    Inventory inventory;
    public Transform itemsParent;
    public GameObject inventoryUI;

    InventorySlot[] slot;

	// Use this for initialization
	void Start () {
		
        inventory = Inventory.instance;
        inventory.onItemChangeCallBack += UpdateUI;

        slot = itemsParent.GetComponentsInChildren<InventorySlot>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Inventory"))
        {
            inventoryUI.SetActive(!inventoryUI.activeSelf);
        }

	}


    void UpdateUI()
    {
        for (int i = 0; i < slot.Length; i++)
        {
            if (i < inventory.items.Count)
            {
                slot[i].AddItem(inventory.items[i]);
            }
            else
            {
                slot[i].ClearSlot();
            }
        }

    }
}
