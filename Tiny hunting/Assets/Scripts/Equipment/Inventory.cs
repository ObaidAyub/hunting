﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public delegate void OnItemChange();
    public OnItemChange onItemChangeCallBack;
    #region Singleton
    public static Inventory instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory Found!");
            return;
        }
        instance = this;
    }

    #endregion
    public int space = 20;

    public List<Item> items = new List<Item>();


    public bool Add(Item item)
    {
        if (!item.isDefaultItem)
        {
            if (items.Count >= space)
            {
                Debug.Log("Not Enough Room");
                return false;
            }
            items.Add(item);

            if(onItemChangeCallBack != null)
                onItemChangeCallBack.Invoke();  
        }
        return true;
    }

    public void Remove(Item item)
    {
        items.Remove(item);

        if(onItemChangeCallBack != null)
            onItemChangeCallBack.Invoke();  
    }
}
