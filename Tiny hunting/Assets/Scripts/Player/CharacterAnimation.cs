﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimation : MonoBehaviour {
    public static CharacterAnimation instance;

    Animator animator;
    NavMeshAgent agent;
    const float locomotionAnimationSmoothTime = 0.1f;

    bool Die;
    private void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
        //float speedPercent = agent.velocity.magnitude / agent.speed;
        //Debug.Log("Speed" + speedPercent);
        //animator.SetFloat("speed ", speedPercent, locomotionAnimationSmoothTime , Time.deltaTime);
    }

    public void  PlayerAttackAnimStart()
    {      
        animator.SetBool("Attack", true);
    }
    public void PlayerAttackAnimStop()
    {
        animator.SetBool("Attack", false);
    }
    public void run(bool istrue)
    {
        animator.SetBool("run", istrue);
    }
    

    public void PlayerDieAnim()
    {
        animator.SetBool("Die", true);
    }
}
