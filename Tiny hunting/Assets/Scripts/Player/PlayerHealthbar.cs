﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthbar : MonoBehaviour {
    Slider healthBar;
    CharacterStats Playerhealth;
    Canvas canvas;
    GameObject target;
    private void Start()
    {
        healthBar = GetComponentInChildren<Slider>();
        Playerhealth = transform.GetComponent<CharacterStats>();
        canvas = GetComponentInChildren<Canvas>();
        target = GameObject.FindGameObjectWithTag("MainCamera");
    }

    void Update()
    {
        canvas.transform.LookAt(target.transform);
        float currenthealth  = (float)(Playerhealth.currentHealth)/ Playerhealth.maxHealth;
        healthBar.value = currenthealth;


    }
}
