using UnityEngine;
using System.Collections.Generic;
using System.Collections;

/* Base class that player and enemies can derive from to include stats. */

public class CharacterStats : MonoBehaviour {

	// Health
	public int maxHealth = 100;
	public int currentHealth { get; private set; }

	public Stat damage;
	public Stat armor;

	// Set current health to max health
	// when starting the game.
	void Awake ()
	{
		currentHealth = maxHealth;
	}

	// Damage the character
	public void TakeDamage (int damage)
	{
        // Subtract the armor value
        Debug.Log("HEre");
        damage -= armor.GetValue();
		damage = Mathf.Clamp(damage, 0, int.MaxValue);

		// Damage the character
		currentHealth -= damage;
  //      if (transform.name!=null)
		//Debug.Log(transform.name + " takes " + damage + " damage.");

		// If health reaches zero
		if (currentHealth <= 0)
		{
            Debug.Log("CHARACTER NAME " + this.gameObject.name);
			Die();
		}
	}

	public virtual void Die ()
	{
		 
        if (transform.tag == "Player")
        {
            PlayerMotor.playerDie = true;
            CharacterAnimation.instance.PlayerDieAnim();
        }
        else if(transform.tag == "Enemy")
        {
            Animator anim = this.gameObject.GetComponentInChildren<Animator>();
            anim.SetBool("Die", true);
           
        }
        else if (transform.tag == "Employee")
        {
            Animator anim = this.gameObject.GetComponentInChildren<Animator>();
            anim.SetBool("Die", true);
            this.gameObject.GetComponent<Prison>().agent.speed = 0;
            this.gameObject.GetComponent<Prison>()._die = true;


        }
        else if (transform.tag == "Gate")
        {
            Debug.Log("Gate Destroyed");

        }

        //StartCoroutine(WaitForDeathAnim());
        
        
        // 


    }
    IEnumerator WaitForDeathAnim()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }
}
