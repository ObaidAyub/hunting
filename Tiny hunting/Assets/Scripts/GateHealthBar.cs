﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GateHealthBar : MonoBehaviour
{
    Slider healthBar;
    CharacterStats health;
    Canvas canvas;
    GameObject target;
    private void Start()
    {
        healthBar = GetComponentInChildren<Slider>();
        health = transform.GetComponent<CharacterStats>();
        canvas = GetComponentInChildren<Canvas>();
        target = GameObject.FindGameObjectWithTag("MainCamera");
    }

    void Update()
    {
        canvas.transform.LookAt(target.transform);
        float currenthealth = (float)(health.currentHealth) / health.maxHealth;
        healthBar.value = currenthealth;
    }
}
